module gitea.com/iota/iota

go 1.16

require (
	gitea.com/iota/toolkit v0.0.0-20211216085837-d28478c33804
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.10.0
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/ugorji/go v1.2.6 // indirect
	golang.org/x/crypto v0.0.0-20220208233918-bba287dce954 // indirect
	golang.org/x/sys v0.0.0-20220207234003-57398862261d // indirect
	google.golang.org/protobuf v1.27.1
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
